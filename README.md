# virtualpath

Your PATH is on fire!

```
♪┏(°.°)┛┗(°.°)┓┗(°.°)┛┏(°.°)┓ ♪
```

Merge your docker image PATH and your own PATH! Then just enjoy~!

-----

In other words, virtualpath lets you use the binaries inside a docker
image's `$PATH` just as if they were in your normal host's `$PATH`.

An interactive bash session can explain better than a thousands words,
so here it is:

```bash
user@host:~/foo$ cat /etc/issue
Arch Linux \r (\l)

user@host:~/foo$ docker images | grep debian
debian                                 jessie [...]
user@host:~/foo$ dpkg-buildpackage --version
bash: dpkg-buildpackage: command not found
user@host:~/foo$ virtualpath create .virtualpath/ debian:jessie
user@host:~/foo$ source .virtualpath/activate.bash
(.virtualpath) user@host:~/foo$ dpkg-buildpackage --version
Debian dpkg-buildpackage version 1.17.27.

This is free software; see the GNU General Public License version 2 or
later for copying conditions. There is NO warranty.
(.virtualpath) user@host:~/foo$ deactivate
user@host:~/foo$ dpkg-buildpackage --version
bash: dpkg-buildpackage: command not found
user@host:~/foo$
```

### Installation

#### Debian/Ubuntu

1. Download the .deb package in the
   [latest tag](https://gitlab.com/ralt/virtualpath/tags)
2. Run `dpkg -i <.deb file>`

#### Arch Linux

1. Download the .pkg.tar.xz file in the
   [latest tag](https://gitlab.com/ralt/virtualpath/tags)
2. Run `pacman -U <.pkg.tar.xz file>`

#### From source

```
$ git clone https://gitlab.com/ralt/virtualpath
$ cd virtualpath
$ cargo build --release
$ cp target/release/virtualpath /usr/bin
```

### Support

For any bug reports, patches, discussions or questions, please send an
email to <virtualpath-project@googlegroups.com>.

#### Shells

As of right now, only bash is supported. zsh and fish support is
planned, if interest is shown. For others, I'm open to discussing it.

#### Container backend

As of right now, only docker is supported. lxc/lxd support is planned,
if interest is shown. For others, I'm open to discussing it.

### How does it work?

virtualpath will mount a FUSE filesystem in a temporary directory and
add this directory to your `$PATH`. In this filesystem is the list of
binaries available in the docker image's `$PATH`, and virtualpathfs
makes sure the executions are forwarded to `docker run`. For more gory
details, you can look at a file's contents in the virtualpathfs
mount.

### How do I make a release?

Note: these are some notes I'm using to remind myself how to make
releases.

```
$ ./release.bash <new version>
```

### License

This software is licensed under the GNU General Public License
version 3. See [LICENSE](LICENSE) for a copy of the license document.
