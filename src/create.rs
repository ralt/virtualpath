use std::fs;
use std::io;
use std::io::Write;
use std::process::exit;
use std::path::Path;
use std::env;

use docker;

const ACTIVATE_SCRIPT: &'static str = include_str!("activate.bash");

pub fn create(path: &str, docker_image: &str) {
    let cwd = match env::current_dir() {
        Ok(cwd) => match cwd.to_str() {
            Some(cwd) => String::from(cwd),
            None => {
                println!("Failed to convert cwd to string");
                exit(-1);
            },
        },
        Err(e) => {
            println!("Failed to get cwd: {}", io::Error::from(e));
            exit(-1);
        },
    };

    // Disallow running virtualpath outside of a limited number of
    // paths, there are too many risks of mixing data with
    // containers'. If users _really_ want to do stupid stuff,
    // let them do it through an environment variable.
    let disable_cwd_check = match env::var("VIRTUALPATH_DISABLE_CWD_CHECK") {
        Ok(_) => true,
        Err(_) => false,
    };

    if !disable_cwd_check {
        let allowed_paths = vec!["/home/", "/tmp/"];
        let mut allowed = false;
        for allowed_path in allowed_paths {
            if cwd.starts_with(allowed_path) {
                allowed = true;
            }
        }

        if !allowed {
            println!("You're trying to create outside of an allowed path.");
            println!("See 'man virtualpath' for more information.");
            exit(-1);
        }
    }


    // The sanest way to make sure the docker image exists
    // is to run a dummy command in it.
    docker::safe_run(docker_image, "ls");

    fs::create_dir_all(path).unwrap_or_else(|why| {
        println!("! {:?}", why.kind());
        exit(-1);
    });

    let mut f = match fs::File::create(
        Path::new(path).join("activate.bash")
    ) {
        Ok(f) => f,
        Err(e) => {
            println!(
                "Failed to create the activate.bash file: {}",
                io::Error::from(e)
            );
            exit(-1);
        },
    };

    match f.write_all(
        ACTIVATE_SCRIPT.replace("{{image}}", docker_image).as_bytes()
    ) {
        Ok(_) => {},
        Err(e) => {
            println!(
                "Failed to write the activate.bash file: {}",
                io::Error::from(e)
            );
            exit(-1);
        },
    }
}
