use std::process::Command;
use std::process::exit;
use std::process::Output;

pub fn safe_run(image: &str, command: &str) -> String {
    let output = run(image, command);

    if !output.status.success() {
        println!("{}", String::from_utf8_lossy(&output.stderr).to_string());
        exit(-1);
    }

    return String::from_utf8_lossy(&output.stdout).to_string();
}

pub fn run(image: &str, command: &str) -> Output {
    return Command::new("docker")
        .args(&["run", "--rm=true", "-i", image, "/bin/bash", "-c", command])
        .output()
        .expect(
            &format!(
                "Failed to start 'docker run' with image '{image}'",
                image=image
            )[..]
        );
}
