use fuse;
use libc;
use time;
use users;
use std;

use docker;

const TTL: time::Timespec = time::Timespec{sec: 1, nsec: 0};

struct VirtualpathFS {
    paths: Vec<String>,
    image: String,
}

fn file_attr(t: time::Timespec, ino: u64, size: usize) -> fuse::FileAttr {
    return attr(t, ino, fuse::FileType::RegularFile, size);
}

fn dir_attr(t: time::Timespec, ino: u64) -> fuse::FileAttr {
    return attr(t, ino, fuse::FileType::Directory, 0);
}

fn attr(t: time::Timespec, ino: u64, kind: fuse::FileType, size: usize) -> fuse::FileAttr {
    return fuse::FileAttr{
        ino: ino,
        size: size as u64,
        blocks: 0,
        atime: t,
        mtime: t,
        ctime: t,
        crtime: t,
        kind: kind,
        perm: 0o500,
        nlink: 2,
        uid: users::get_current_uid(),
        gid: users::get_current_gid(),
        rdev: 0,
        flags: 0,
    };
}

const RUN_SCRIPT: &'static str = include_str!("run.py");

fn file_content(image: &String) -> String {
    return RUN_SCRIPT.replace("{{image}}", image.as_str());
}

impl fuse::Filesystem for VirtualpathFS {
    fn lookup(&mut self, _req: &fuse::Request, parent: u64, name: &std::path::Path, reply: fuse::ReplyEntry) {
        if parent == 1 {
            for (idx, path) in self.paths.iter().enumerate() {
                if Some(path.to_string().as_str()) == name.to_str() {
                    reply.entry(
                        &TTL,
                        &file_attr(
                            time::now().to_timespec(),
                            idx as u64 + 1,
                            file_content(&self.image).len()
                        ),
                        0
                    );
                    return;
                }
            }
        }
        reply.error(libc::ENOENT);
    }

    fn getattr(&mut self, _req: &fuse::Request, ino: u64, reply: fuse::ReplyAttr) {
        let now = time::now().to_timespec();
        match ino {
            0 => reply.error(libc::ENOENT),
            1 => reply.attr(&TTL, &dir_attr(now, ino)),
            _ => {
                if ino < self.paths.len() as u64 {
                    reply.attr(&TTL, &file_attr(now, ino, file_content(&self.image).len()));
                }
                else {
                    reply.error(libc::ENOENT);
                }
            },
        }
    }

    fn read(&mut self, _req: &fuse::Request, ino: u64, _fh: u64, offset: u64, _size: u32, reply: fuse::ReplyData) {
        if ino == 0 || ino as usize >= self.paths.len() {
            reply.error(libc::ENOENT);
            return;
        }

        reply.data(file_content(&self.image)[offset as usize..].as_bytes());
    }

    fn readdir(&mut self, _req: &fuse::Request, ino: u64, _fh: u64, offset: u64, mut reply: fuse::ReplyDirectory) {
        if ino == 1 {
            if offset == 0 {
                reply.add(1, 0, fuse::FileType::Directory, ".");
                reply.add(1, 1, fuse::FileType::Directory, "..");

                let mut iter_offset: u64 = 2;
                for (idx, path) in self.paths.iter().enumerate() {
                    if idx == 0 {
                        // Skip the first inode being the directory.
                        continue;
                    }

                    reply.add(
                        idx as u64 + 1,
                        iter_offset,
                        fuse::FileType::RegularFile,
                        path
                    );

                    iter_offset += 1;
                }
            }
            else {
                let mut iter_offset: u64 = offset + 1;
                for (idx, path) in self.paths.iter().enumerate() {
                    if idx < offset as usize {
                        // Skip until the offset
                        continue;
                    }
                    reply.add(
                        idx as u64 + 1,
                        iter_offset,
                        fuse::FileType::RegularFile,
                        path
                    );

                    iter_offset += 1;
                }
            }
            reply.ok();
        } else {
            reply.error(libc::ENOENT);
        }
    }
}

pub fn activate(docker_image: &str, fuse_folder: &str) {
    let mut fs = VirtualpathFS{
        paths: vec![],
        image: String::from(docker_image),
    };
    // The fs holds a list of all the items in the filesystem,
    // and the inode is the index in fs.paths + 1, so the first
    // item should be the main directory.
    fs.paths.push(String::from(fuse_folder));

    // And the items in the docker image's path are the remaining ones.
    let output = docker::safe_run(docker_image, "compgen -c");
    for line in output.lines() {
        fs.paths.push(line.to_string());
    }

    fuse::mount(fs, &fuse_folder, &[]);
}
