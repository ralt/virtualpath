extern crate clap;
extern crate fuse;
extern crate time;
extern crate libc;
extern crate users;

use clap::{Arg, App, SubCommand};

mod docker;
mod create;
mod activate;
mod deactivate;

fn main() {
    let matches = App::new("virtualpath")
        .help(r"Usage: virtualpath create <folder> <docker image>

Once you have created the folder, you can activate your virtualpath with:

  source <folder>/activate.bash

To exit the virtualpath, you can run:

  deactivate

More information in 'man virtualpath'.

If you find any bug, please send an email to <virtualpath-project@googlegroups.com>")
        .subcommand(SubCommand::with_name("create")
                    .about("create a virtualpath folder")
                    .arg(Arg::with_name("path").takes_value(true))
                    .arg(Arg::with_name("docker image").takes_value(true)))
        .subcommand(SubCommand::with_name("activate")
                    .about("activate a virtualpath")
                    .arg(Arg::with_name("docker image").takes_value(true))
                    .arg(Arg::with_name("fuse folder").takes_value(true)))
        .subcommand(SubCommand::with_name("deactivate")
                    .about("deactivate a virtualpath")
                    .arg(Arg::with_name("directory").takes_value(true)))
        .get_matches();

    if let Some(matches) = matches.subcommand_matches("create") {
        match matches.value_of("path") {
            Some(path) => match matches.value_of("docker image") {
                Some(docker_image) => create::create(path, docker_image),
                None => println!("Docker image is required."),
            },
            None => println!("Path is required."),
        }
    }

    if let Some(matches) = matches.subcommand_matches("activate") {
        match matches.value_of("docker image") {
            Some(docker_image) => match matches.value_of("fuse folder") {
                Some(fuse_folder) => activate::activate(
                    docker_image,
                    fuse_folder
                ),
                None => println!("Fuse folder is required."),
            },
            None => println!("Docker image is required."),
        }
    }

    if let Some(matches) = matches.subcommand_matches("deactivate") {
        match matches.value_of("directory") {
            Some(directory) => deactivate::deactivate(directory),
            None => println!("Directory to deactivate is required."),
        }
    }
}
