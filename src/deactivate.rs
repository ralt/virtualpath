use std::process::Command;
use std::process::exit;

pub fn deactivate(directory: &str) {
    let output = Command::new("fusermount")
        .args(&["-u", directory])
        .output()
        .expect("fusermount failed to start");

    if !output.status.success() {
        println!("{}", String::from_utf8_lossy(&output.stderr).to_string());
        exit(-1);
    }
}
