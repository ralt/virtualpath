deactivate () {
    virtualpath deactivate "$_virtualpath_fuse_directory"
    export PATH="$_virtualpath_old_path"
    unset _virtualpath_old_path

    rm -rf "$_virtualpath_fuse_directory"
    unset _virtualpath_fuse_directory

    unset _virtualpath_activate_dir

    export PS1="$_virtualpath_old_ps1"
    unset _virtualpath_old_ps1

    unset -f run_in_virtualpath
    unset -f deactivate
}

run_in_virtualpath () {
    # Convenience function to run a command in the VP.
    program="$1"
    shift
    "$_virtualpath_fuse_directory"/"$program" "$@"
}

export _virtualpath_activate_dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

export _virtualpath_old_path="$PATH"

export _virtualpath_fuse_directory="$(mktemp -d -t virtualpath-XXXXXXXXXX)"

docker_image="{{image}}"

(virtualpath activate "$docker_image" "$_virtualpath_fuse_directory" \
            >> "$_virtualpath_activate_dir/logs" 2>&1 \
    &)

export PATH="$PATH:$_virtualpath_fuse_directory"

export _virtualpath_old_ps1="$PS1"

export PS1="($(basename "$_virtualpath_activate_dir")) $PS1"
