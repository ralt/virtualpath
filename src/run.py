#!/usr/bin/env python

# Python is used here because it's installed on every Linux system.
# A better way at some point would be to switch to generated ELF binary.
# Meanwhile, the code here has to be compatible with py2 and py3.

import errno
import os
import shlex
import sys
import tempfile
import uuid

fd, temp_file = tempfile.mkstemp()

pid = os.fork()
if pid == 0:
    def ssh_socket_folder():
        try:
            absolute_socket_path = os.readlink(os.environ["SSH_AUTH_SOCK"])
        except KeyError:
            # No SSH_AUTH_SOCK env var.
            return None
        except OSError as e:
            if e.errno != errno.EINVAL:
                raise
            absolute_socket_path = os.environ["SSH_AUTH_SOCK"]
        return os.path.dirname(absolute_socket_path)

    mounted_folder = os.path.dirname(os.environ["_virtualpath_activate_dir"])

    vars_to_ignore = ["PATH", "COLUMNS", "SHELL"]
    for k, v in os.environ.items():
        if k in vars_to_ignore:
            continue
        line = '{}={}\n'.format(k, v)
        os.write(fd, line.encode("utf-8"))

    volumes = []

    volumes.append((mounted_folder, mounted_folder))

    ssh_socket = ssh_socket_folder()
    if ssh_socket is not None:
        volumes.append((ssh_socket, ssh_socket))

    filename = os.path.basename(sys.argv[0])

    args = [
        "docker", "run",
        "--rm=true",
    ]

    for local_folder, container_folder in volumes:
        args += ["-v", "{}:{}".format(
            local_folder,
            container_folder,
        )]

    # You really want to run a command not as root, but as
    # a normal user with the same uid/gid as you have, or
    # it messes up the written files. However, you probably
    # also want the 'root' group to be able to run whatever
    # you want.
    current_uid = os.getuid()
    current_gid = os.getgid()
    current_home = os.getenv("HOME")

    user_in_docker = uuid.uuid4().hex
    with_temp_user = ";".join([
        "groupadd --gid {} --non-unique {}".format(
            current_gid, user_in_docker,
        ),
        "useradd --home-dir {} --uid {} --gid {} --non-unique {}".format(
            current_home, current_uid, current_gid, user_in_docker,
        ),
        "usermod -a -G {} {}".format(user_in_docker, user_in_docker),
        "usermod -a -G root {}".format(user_in_docker),
        "chown -R {}:{} {}".format(
            user_in_docker, user_in_docker, current_home,
        ),
        "su --preserve-environment --command {{command}} {}".format(
            user_in_docker,
        ),
    ])

    args += [
        "--env-file", temp_file,
        "-ti", "{{image}}",
        "/bin/bash", "-c",
        with_temp_user.replace("{command}", "'cd {}; {}'".format(
            os.getcwd(),
            " ".join([filename] + [shlex.quote(arg) for arg in sys.argv[1:]]),
        )),
    ]

    os.execvp("docker", args)
else:
    os.wait()
    os.close(fd)
    os.unlink(temp_file)
