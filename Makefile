all:
	cargo build --release
	pandoc -s -t man docs/virtualpath.md | gzip > target/release/virtualpath.1.gz

.PHONY: debian-package arch-package clean

debian-package:
	dpkg-buildpackage -b -us -uc
	mv ../*.deb .
	make clean # Necessary to clean up files created as root in the container.

arch-package:
	makepkg

clean:
	rm -rf target

dist-clean:
	rm -rf pkg *.asc *.deb *.xz virtualpath target
