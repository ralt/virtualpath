FROM debian:jessie

MAINTAINER Florian Margaine <florian@margaine.com>

RUN apt-get update --fix-missing

RUN apt-get -y install libfuse-dev devscripts build-essential sudo fuse git-buildpackage pkg-config pandoc locales-all

RUN apt-get upgrade -y && apt-get dist-upgrade -y

RUN curl -sSf https://static.rust-lang.org/rustup.sh | sh

RUN rm -rf /var/lib/apt/lists/*
