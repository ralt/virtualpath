#!/usr/bin/env bash

set -e

VERSION="$1"

if [ -z "$VERSION" ]; then
    echo "Usage: ./release.bash <version>"
    exit 0
fi

make clean

docker build -t virtualpath .

virtualpath create .vp/ virtualpath:latest

source .vp/activate.bash
sleep 2

git-dch --debian-tag="%(version)s" -R -N "$VERSION" --spawn-editor="never"

git add debian/changelog

git commit --sign --message "$(run_in_virtualpath dpkg-parsechangelog -S Changes -n1)"

git tag --annotate --sign --message "$(run_in_virtualpath dpkg-parsechangelog -S Changes -n1)" "$VERSION"

git push origin master

git push origin --tags

run_in_virtualpath make debian-package

make arch-package

gpg --detach-sign --armor --output virtualpath_"$VERSION"_amd64.asc virtualpath_"$VERSION"_amd64.deb

gpg --detach-sign --armor --output virtualpath-"$VERSION"-1-x86_64.pkg.asc virtualpath-"$VERSION"-1-x86_64.pkg.tar.xz
