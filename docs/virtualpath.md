% VIRTUALPATH(1) virtualpath man page
% Florian Margaine <florian@margaine.com>
% September 29, 2016

# NAME

virtualpath - Your PATH is on fire!

# SYNOPSIS

virtualpath create <folder> <docker image>

# DESCRIPTION

Merge your docker image PATH and your own PATH! Then just enjoy~!

In other words, virtualpath lets you use the binaries inside a docker
image's `$PATH` just as if they were in your normal host's `$PATH`.

Once you have created the folder with `virtualpath create`, you can
activate your virtualpath with:

        source <folder>/activate.bash

The binaries available in the docker image will now be runnable from
your bash session.

To exit the virtualpath, you just need to run:

        deactivate

`virtualpath` tries to not override the binaries in your host's
`$PATH`, so if a binary exists in both the docker image and in your
host, your host's will be used by default. If you want to force using
the one in your docker image, you can run:

        run_in_virtualpath <docker image's binary> [arguments]

# Disallowed paths

To protect you from overriding paths in the docker containers,
`virtualpath create` will prevent you from creating a virtualpath
outside of `/home` and `/tmp`. You can bypass this by having a
`VIRTUALPATH_DISABLE_CWD_CHECK` environment variable with any value.

# HELP

Get help and report bugs at <virtualpath-project@googlegroups.com>
